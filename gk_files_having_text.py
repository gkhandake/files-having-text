#!/usr/bin/env python3

__author__ = "Gajanan Khandake"
__project__ = "https://gitlab.com/gkhandake/files-having-text"
__copyright__ = "Copyright 2020"
__license__ = "MIT"
__version__ = "1.0.0"

import sys, getopt
import os, glob
from inspect import getframeinfo, stack
import time

__debug = False

def __cprint(category, message):
    _caller = getframeinfo(stack()[2][0])
    for line in message.splitlines():
        print( "%s::%s::%s::%d::%s" % (time.time() * 1000, category, _caller.filename, _caller.lineno, line))

def __iprint(message):
    __cprint("INFO", message)

def __eprint(message):
    __cprint("ERROR", message)

def __dprint(message):
    if(__debug) :
        __cprint("DEBUG", message)

def __lookup(search, directory, include, exclude):
    message = "Searching: " + search + "\n"
    message += "In directory : " + directory + "\n"
    message += "Having extensions: "+ ', '.join(include) + "\n"
    message += "By excluding: " + ', '.join(exclude)
    __dprint(message)

    for root, dirnames, files in os.walk(directory):
        dirnames [:] = [dirname for dirname in dirnames  if dirname not in exclude]
        for file in files:
            if len(include) == 0 or file.endswith(include):
                _file_with_path = os.path.join(root, file)
                with open(_file_with_path, 'r', encoding='utf-8') as f:
                    if search in f.read():
                        print(_file_with_path)

def __main(program, argv):
    _usage = program + ' -s <search>  [-d <directory> -i <files extensions (comma separated)>  -e <directories (comma separated)>]'
    _search = ""
    _directory = "./"
    _include = tuple()
    _exclude = tuple()

    try:
        _options, _arguments = getopt.getopt(argv, "hs:d:i:e:", ["search=", "directory=", "include=", "exclude="])
    except getopt.GetoptError:
        print(_usage)
        sys.exit(2)
    
    for _option, arg in _options:
        if _option == '-h':
            print(_usage)
            sys.exit()
        elif _option in ("-s", "--search"):
            _search = arg
        elif _option in ("-d", "--directory"):
            _directory = arg
        elif _option in ("-i", "--include"):
            _include = tuple(arg.split(","))
        elif _option in ("-e", "--exclude"):
            _exclude = tuple(arg.split(","))

    if _search == "":
        __iprint("Please specify search to lookup through -c option")
        sys.exit()

    __lookup(_search, _directory, _include, _exclude)

if __name__ == "__main__":
    __main(sys.argv[0], sys.argv[1:])